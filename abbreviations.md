To shorten names and make them universal abbreviations must be used. All standard abbreviations are listed in the table below. If some term is not listed in the table, consult commonly used abbreviations in popular databases like www.allacronyms.com or www.abbreviations.com. Any newly introduced commonly used abbreviations should be added to the table below.

|Abbreviation|Term|Context|
|---|---|---|
|a|advanced|Prefix of the library (library's namespace)|
|adv|advanced|Part of the library name|
|advc|Advanced C|The library name|
|alloc|allocate|Memory allocation|
|avg|average|Average value|
|cmp|compare|Compare strings, in other cases the full word should be used|
|curr|current|Current object, commonly used in loops|
|dir|directory|File system directory|
|e|error|Variables that store error number (errno_t)|
|err|error|Names related to errors|
|fd|file descriptor|Numeric file descriptor|
|fin|finalize|Free object's internal resources, but don't free the object itself|
|info|information|Common name of structs with descriptive data|
|io|input/output|Input and output operations|
|len|length|Object length (string or array), commonly not used to store size in bytes|
|min|minimum|Minimum value|
|max|maximum|Maximum value|
|mem|memory|Operating memory|
|nt|null-terminated|Null-terminated array|
|ntstr|null-terminated string|Null-terminated strings|
|prev|previous|Previous object, commonly used in linked lists|
|ptr|pointer|memory pointers|
|str|string|str_t type|
|sys|system|Anything related to operating system|
