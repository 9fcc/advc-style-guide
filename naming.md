The table below lists standard names for functions (methods) depending on what the function should do.

|Name|Context|
|---|---|
|close|Close the connection or object, usually closes file descriptors.|
|fin|Finalize object, but don't free it (free all internal structures). Finalized object has no pointers to allocated memory.|
|init|Initialize object (without memory allocation for its main structure).|
|free|Finalize the object and free its memory.|
|new|Allocate the memory and initialize the object.|
|open|Open the connection or object.|
|set|Set new value by copying it (with memory allocations if needed).|
|take|Take new value by pointer and store it in the object (value is not copied).|

Label names for parts of the function should start with the function section name. Standard names for the sections are listed in the table below:

|Name|Context|
|---|---|
|undo|Undo section. Every operation is undone in reverse order. Used to free all resource allocations if error has encountered.|
|clean|Cleaning section with freeing allocated resources after function finished its work. Used to skip part of function and return.|

Standard variable names that used as the whole or a part of variable name:

|Name|Context|
|---|---|
|e|Error code of type errno_t.|
|fd|File descriptor for type a_filedes_t.|
|out|Used at the end of function argument, which is pointer that used as return value of the function.|
